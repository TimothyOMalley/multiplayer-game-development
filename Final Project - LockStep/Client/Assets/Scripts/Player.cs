﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    float speed = 5;
    ClientNetwork clientNet;

    private void Awake() {
        clientNet = GameObject.FindGameObjectWithTag("ExampleClient").GetComponent<ClientNetwork>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NetworkSync>().owned)
        {
            //if (clientNet.playing) {
                Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime, 0);
                transform.position += movement;
            //}
        }
    }
}
