﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CameraController))]
//This is the primary player script; so I have this also add Movment, Rigidbody (for movement) and CameraController

public class PlayerInput : MonoBehaviour{

    enum playernum { one, two, three, four, playerSelect} //Enum setup for which player to control
    playernum player = playernum.one; //by default the player is player one

    PlayerSelecter selecter_; 
    Movement move;

    void Awake() {

        switch (gameObject.tag){
            case "one":
                player = playernum.one;
                gameObject.GetComponent<Renderer>().material.color = Color.green;
                break;

            case "two":
                player = playernum.two;
                gameObject.GetComponent<Renderer>().material.color = Color.red;
                break;

            case "three":
                player = playernum.three;
                gameObject.GetComponent<Renderer>().material.color = Color.blue;
                break;

            case "four":
                player = playernum.four;
                gameObject.GetComponent<Renderer>().material.color = Color.black;
                break;

            default: //if not a player, then activat the selecter (only on select screen)
                //I did this because the select screen needs player's inputs aswell

                player = playernum.playerSelect;
                selecter_ = GameObject.FindGameObjectWithTag("PlayerSelecter").GetComponent<PlayerSelecter>();
                break;
        } //determines the player based on the tag of the player. Additionally sets the color of the player 

        move = gameObject.GetComponent<Movement>(); 
    }

    void Update(){

        switch (player){ //determines which player this is for which contoller scheme to use
            case playernum.one:
                //once the player has been determined, it then sends the horizontal and vertical input for that player
                //to the movement class.
                move.GiveInput(new Vector2(Input.GetAxis("HorizontalOne"),Input.GetAxis("VerticalOne")));
                break;

            case playernum.two:
                move.GiveInput(new Vector2(Input.GetAxis("HorizontalTwo"), Input.GetAxis("VerticalTwo")));
                break;

            case playernum.three:
                move.GiveInput(new Vector2(Input.GetAxis("HorizontalThree"), Input.GetAxis("VerticalThree")));
                break;

            case playernum.four:
                move.GiveInput(new Vector2(Input.GetAxis("HorizontalFour"), Input.GetAxis("VerticalFour")));
                break;

            case playernum.playerSelect: //Player select is essentially just looking for players to hit their "join" button
                if (Input.GetButtonDown("ActivateOne"))
                    selecter_.GivePlayers(0);

                if (Input.GetButtonDown("ActivateTwo"))
                    selecter_.GivePlayers(1);

                if (Input.GetButtonDown("ActivateThree"))
                    selecter_.GivePlayers(2);

                if (Input.GetButtonDown("ActivateFour"))
                    selecter_.GivePlayers(3);

                if (Input.GetButtonDown("Start")) //This attempts to start the game, if enough players are present, the game will start
                    selecter_.StartGame();

                break;
        } //this handels all inputs from movement to joining player select screen
    }
}
