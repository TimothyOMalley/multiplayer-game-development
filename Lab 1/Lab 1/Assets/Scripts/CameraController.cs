﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour{
    GameManager brain;
    Camera cam;
    // Start is called before the first frame update
    void Start(){
        cam = gameObject.GetComponentInChildren<Camera>();
        brain = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        cam.enabled = false;
    }

    public void SetCameras(){

        //Sets different aspect ratios for the split-screen cameras based on how many players there are

        switch (brain.GetNumberOfPlayers()) { //first it finds how many players there are

            case 2:
                if (gameObject.tag == "one"){ //Then it determines which player's camera it is
                    cam.rect = new Rect(0, .5f, 1, .5f); //then it sets the aspect ratio for the character
                }

                else if (gameObject.tag == "two"){
                    cam.rect = new Rect(0, 0, 1, .5f);
                }

                break;


            case 3:
                if (gameObject.tag == "one"){
                    cam.rect = new Rect(0, .5f, .5f, .5f);
                }

                else if (gameObject.tag == "two"){
                    cam.rect = new Rect(0, 0, 1, .5f);
                }

                else if (gameObject.tag == "three"){
                    cam.rect = new Rect(.5f, .5f, .5f, .5f);
                }

                break;


            case 4:
                if (gameObject.tag == "one"){
                    cam.rect = new Rect(0, .5f, .5f, .5f);
                }

                else if (gameObject.tag == "two"){
                    cam.rect = new Rect(0, 0, .5f, .5f);
                }

                else if (gameObject.tag == "three"){
                    cam.rect = new Rect(.5f, .5f, .5f, .5f);
                }

                else if (gameObject.tag == "four"){
                    cam.rect = new Rect(.5f, 0, .5f, .5f);
                }

                break;
        }
    }
}
