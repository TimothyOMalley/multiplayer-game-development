﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerSelecter : MonoBehaviour
{
    GameManager brain;
    GameObject startButton;

    
    void Start(){
        brain = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        startButton = GameObject.FindGameObjectWithTag("StartButton");
        startButton.SetActive(false); //I don't want the players to be able to start the game until there's
        //enough players to play
    }

    public void GivePlayers(int slot){

        if (brain.GetActivePlayerInSlot(slot) == false) //if a slot is empty, a player can join on it
            brain.AddPlayer(slot);

        if (brain.GetNumberOfPlayers() >= 2) //if there is 2 or more players activate the start button
            startButton.SetActive(true);
        
    }

    public void StartGame(){

        if (startButton.activeInHierarchy) //if the start button is active, start on button press
            brain.ChangeLevel("PlayTesting"); //change the level
    }
}
