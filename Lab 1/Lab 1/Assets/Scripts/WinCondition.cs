﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{
    bool first = true;

    private void OnTriggerEnter(Collider other) {
        if (first){

            first = false; //I don't want another player hitting it after and getting results

            //I know this line isn't super clean, but this was just my lazy way of accessing the GameManager 
            foreach( GameObject n in GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetCharacters())
            {
                try{ //Because I deleted all other players that weren't selected in the start of the game,
                    //I needed to not look at them in this step otherwise it'd break
                    n.GetComponent<Renderer>().material.color = Color.gray;
                }
                catch (MissingReferenceException) { }
            }

            other.GetComponent<Renderer>().material.color = Color.yellow; //this sets the winner to yellow
        }
    }
}
