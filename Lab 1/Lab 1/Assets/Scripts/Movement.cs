﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]

public class Movement : MonoBehaviour
{
    [SerializeField] float speedMod = 4.0f;
    [SerializeField] float rotationSpeed = 50f;
    private Vector2 movement;
    private Rigidbody character;

    void Awake(){
        character = GetComponent<Rigidbody>();
    }

    void Update(){
        transform.Rotate(transform.up * movement.x * rotationSpeed * Time.deltaTime); //turn with horizontal input
    }

    private void FixedUpdate(){ //move forward/backwards with vertical input
        character.MovePosition(character.position + (transform.forward * movement.y * speedMod * Time.fixedDeltaTime));
    }

    public void GiveInput(Vector2 input_) { //what Playerinput calls to give input information to movement
        movement = input_;
        if (movement.magnitude >= 1.0f)
            movement = movement.normalized;
    }
}
