﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour{ 
    [SerializeField] int players = 0; //some serialized fields for debugging purposes
    [SerializeField] GameObject[] characters;
    bool[] playersPlaying = new bool[4] { false, false, false, false }; 

    private void Awake(){
        characters = new GameObject[4] {
            GameObject.FindGameObjectWithTag("one"),
            GameObject.FindGameObjectWithTag("two"),
            GameObject.FindGameObjectWithTag("three"),
            GameObject.FindGameObjectWithTag("four")
        }; //finds and assigns all players

        foreach(GameObject n in characters) { n.SetActive(false); }
        //then it sets each character's activity state to false for character select
    }

    public int GetNumberOfPlayers(){ return players; }

    public void ChangeLevel(string level){ //I have the GameMAnager handeling the level change
        //because I wanted to make sure certain things (Such as the GameManager) don't get deleted

        DontDestroyOnLoad(gameObject); //I don't want the GameManager deleted because I want to keep the stored data
        
        foreach(GameObject n in characters) {

            if (n.activeInHierarchy){ 
                DontDestroyOnLoad(n); //This makes sure that only selected characters stay
                n.GetComponentInChildren<Camera>().enabled = true; //Also enables the cameras which were
                //initally disabled as to not mess with the character select screen camera
            }
        } //keeps characters from character select screen

        SceneManager.LoadScene(level);
        
        foreach(GameObject n in characters){
            if (n.activeInHierarchy){
                n.GetComponent<CameraController>().SetCameras();
            }
        } //I wanted the cameras to set themselves up AFTER level load
        
    }

    public void AddPlayer(int slot){ 
        players++;
        playersPlaying[slot] = true; //this is setup so a player can't activate twice

        characters[slot].SetActive(true); //activates the player

    } //This adds a player to the playing players when called

    public bool GetActivePlayerInSlot(int slot){
        return playersPlaying[slot];
    } //returns the activity state of the player's slot

    public GameObject[] GetCharacters()
    {
        return characters;
    } //return the character array
}
