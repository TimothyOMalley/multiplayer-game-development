﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour{
    [SerializeField] Text plClass;
    [SerializeField] ClientNetwork clientNet;
    [SerializeField] GameObject characterSelectScreen;

    public void Select() {
        clientNet.CallRPC("SetPlayerClass", UCNetwork.MessageReceiver.ServerOnly, -1, plClass.text);
        characterSelectScreen.SetActive(false);
    }
}
