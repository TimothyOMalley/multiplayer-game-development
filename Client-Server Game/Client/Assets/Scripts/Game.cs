﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {
    bool controlCamera = true;
    public float health = 0;
    [SerializeField] ClientNetwork clientNet;
    public GameObject myPlayer;

    private void Update() {
        if (controlCamera) { //when dead, let the player move the camera to spectate
            gameObject.transform.position += new Vector3(Input.GetAxis("Horizontal") * -5 * Time.deltaTime, Input.GetAxis("Vertical") * 5 * Time.deltaTime,0);
        } else {
        }
    }

    public void ResetCamera() {
        gameObject.transform.position = new Vector3(0, 1, -53.2f);
        controlCamera = false;
    }

    public void UpdateHealth(float change) {
        health += change;
    }

    public void SetStartingHealth(float shealth) {
        health = shealth;
    }

    public void ProjectileHit() {
        //soundbyte or visual queue
    }

    public void RoundEnd() {
        KillPlayer();
        controlCamera = true;
    }

    public void RoundStart() {
        ResetCamera();
        clientNet.CallRPC("MakePlayerServerSide", UCNetwork.MessageReceiver.ServerOnly, -1, Vector3.zero, Quaternion.identity);
        myPlayer = clientNet.Instantiate("Player", Vector3.zero, Quaternion.identity);
        myPlayer.GetComponent<NetworkSync>().AddToArea(1);
    }
    void OnNetStatusDisconnecting() {
        Debug.Log("OnNetStatusDisconnecting called");

        if (myPlayer) {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
    }
    void OnNetStatusDisconnected() {

        if (myPlayer) {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
    }

    public void KillPlayer() {
        if (myPlayer) {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
        try {
            myPlayer.GetComponent<Player>().Kill();
        }catch(Exception e) { }
    }

    void OnDestroy() {
        if (myPlayer) {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
        if (clientNet.IsConnected()) {
            clientNet.Disconnect("Peace out");
        }
    }

}
