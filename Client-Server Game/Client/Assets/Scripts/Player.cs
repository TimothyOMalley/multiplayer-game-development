﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    float speed = 5;
    public GameObject melee, exampleClient, camera;

    private void Awake() {
        exampleClient = GameObject.FindGameObjectWithTag("ExampleServer");

        camera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NetworkSync>().owned)
        {
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime, 0);
            transform.position += movement;
            camera.transform.position += movement;
            

            // https://answers.unity.com/questions/10615/rotate-objectweapon-towards-mouse-cursor-2d.html?_ga=2.203794325.468425127.1607374820-1570257376.1607098196
            //looked up a way to do this and found the solution on an answers forum, and it works really well, so here's the source ^
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 53.2f; //The distance between the camera and object
            Vector3 objectPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            mousePos.x = mousePos.x - objectPos.x;
            mousePos.y = mousePos.y - objectPos.y;
            float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

            exampleClient.GetComponent<ClientNetwork>().CallRPC("PlayerInput", UCNetwork.MessageReceiver.ServerOnly, -1, gameObject.transform.position, gameObject.transform.rotation);

            if (Input.GetButtonDown("Fire1")) { //Melee
                exampleClient.GetComponent<ClientNetwork>().CallRPC("MakeMeleeAttack", UCNetwork.MessageReceiver.ServerOnly, -1);
            }

            if (Input.GetButtonDown("Fire2")) { //Ranged
                Debug.DrawRay(gameObject.transform.position, transform.right, Color.green, 200);
                exampleClient.GetComponent<ClientNetwork>().CallRPC("ShootAtDirection",UCNetwork.MessageReceiver.ServerOnly, -1);
            }
        }
    }

    public void Kill() {
        Destroy(gameObject);
    }
}
