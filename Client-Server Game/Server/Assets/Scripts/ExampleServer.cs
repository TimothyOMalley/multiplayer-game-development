﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class ExampleServer : MonoBehaviour
{
    public static ExampleServer instance;

    public ServerNetwork serverNet;

    public int portNumber = 603;

    public bool gameInProgress = false;

    public GameObject playerPrefab;
    
    // Stores a player
    class Player
    {
        public long clientId;
        public string playerName;
        public bool isReady;
        public bool isConnected;
        public int defense, speed;
        public GameObject plyr;
        public float health;
    }
    List<Player> players = new List<Player>();
    int currentActivePlayer;


    // Use this for initialization
    void Awake()
    {
        instance = this;

        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null)
        {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null)
        {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        //serverNet.EnableLogging("rpcLog.txt");
    }

    // A client has just requested to connect to the server
    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data)
    {
        Debug.Log("Connection request from " + data.username);

        // We either need to approve a connection or deny it
        //if (players.Count < 2)
        {
            Player newPlayer = new Player();
            newPlayer.clientId = data.id;
            newPlayer.playerName = data.username;
            newPlayer.isConnected = false;
            newPlayer.isReady = false;
            newPlayer.plyr = null;
            newPlayer.speed = 0;
            newPlayer.defense = 0;
            newPlayer.health = 0;
            players.Add(newPlayer);

            serverNet.ConnectionApproved(data.id);
        }
        /*
        else
        {
            serverNet.ConnectionDenied(data.id);
        }
        */
    }

    void OnClientConnected(long aClientId)
    {
        // Set the isConnected to true on the player
        foreach (Player p in players)
        {
            if (p.clientId == aClientId)
            {
                p.isConnected = true;
            }
        }
    }

    public void SetPlayerClass(string className) {
        foreach (Player p in players) {
            if(p.clientId == serverNet.SendingClientId) {
                switch (className) {
                    case "Archer":
                        p.speed = 5;
                        p.defense = 5;
                        p.isReady = true;
                        p.health = 50;
                        serverNet.CallRPC("SetStartingHealth", p.clientId, -1, 50);
                        break;
                    case "Assassin":
                        p.speed = 7;
                        p.defense = 2;
                        p.health = 25;
                        serverNet.CallRPC("SetStartingHealth", p.clientId, -1, 25);
                        p.isReady = true;
                        break;
                    case "Tank":
                        p.speed = 3;
                        p.defense = 10;
                        p.health = 75;
                        serverNet.CallRPC("SetStartingHealth", p.clientId, -1, 75);
                        p.isReady = true;
                        break;
                    default:
                        p.speed = 0;
                        p.defense = 0;
                        p.health = 0;
                        p.isReady = false;
                        Debug.Log("Error: Class Doesn't exist");
                        break;
                }
            }
        }
        if (gameInProgress == false) {
            CheckIfPlayable();
        }
    }

    public void PlayerInput(Vector3 pos, Quaternion dir) {
        foreach(Player p in players) {
            if(p.clientId == serverNet.SendingClientId) {
                if (p.plyr != null) {
                    p.plyr.transform.position = pos;
                    p.plyr.transform.rotation = dir;
                } else {
                    Debug.Log("Player doesn't have object to move");
                }
            }
        }
    }

    public void MakeMeleeAttack() {
        foreach (Player p in players) {
            if (p.clientId == serverNet.SendingClientId) {
                RaycastHit hit;
                //Debug.DrawRay(p.plyr.transform.position, p.plyr.transform.TransformDirection(Vector3.right), Color.black, 400);
                if (Physics.Raycast(p.plyr.transform.position, p.plyr.transform.TransformDirection(Vector3.right), out hit, 1)) {
                    if (hit.collider != null) {
                        foreach (Player pHit in players) {
                            if (hit.collider.gameObject == pHit.plyr) {
                                serverNet.CallRPC("UpdateHealth", pHit.clientId, -1, -25);
                                pHit.health -= 5;
                                if(pHit.health <= 0) {
                                    PlayerDies(pHit.clientId);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void ShootAtDirection() {
        foreach(Player p in players) {
            if(p.clientId == serverNet.SendingClientId) {
                RaycastHit hit;
                //Debug.DrawRay(p.plyr.transform.position, p.plyr.transform.TransformDirection(Vector3.right), Color.black, 400);
                if (Physics.Raycast(p.plyr.transform.position, p.plyr.transform.TransformDirection(Vector3.right), out hit, 20)){
                    if (hit.collider != null) {
                        foreach(Player pHit in players) {
                            if(hit.collider.gameObject == pHit.plyr) {
                                serverNet.CallRPC("UpdateHealth", pHit.clientId, -1, -5);
                                pHit.health -= 5;
                                if (pHit.health <= 0) {
                                    PlayerDies(pHit.clientId);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void MakePlayerServerSide(Vector3 pos, Quaternion rot) {
        Debug.Log("MakePlayerServerSide Called");
        foreach(Player p in players) {
            Debug.Log("Comparing "+p.clientId+" to "+serverNet.SendingClientId);
            if(p.clientId == serverNet.SendingClientId) {
                p.plyr = Instantiate(playerPrefab, pos, rot);
            }
        }
    }

    public void CheckIfPlayable() {
        int yes = 0;
        foreach(Player p in players) {
            if (p.isConnected) {
                if (p.isReady) {
                    yes++;
                }
            }
        }
        if(yes >= 2) {
            foreach (Player p in players) {
                if (p.isReady) {
                    serverNet.CallRPC("RoundStart", p.clientId, -1);
                }
            }
        }
    }

    public void PlayerDies(long aClientId) {
        foreach(Player p in players) {
            if(p.clientId == aClientId) {
                Destroy(p.plyr);
                serverNet.CallRPC("KillPlayer", p.clientId, -1);
            }
        }
        CheckRemainingPlayers();
    }

    void OnClientDisconnected(long aClientId)
    {
        // Set the isConnected to true on the player
        foreach (Player p in players)
        {
            if (p.clientId == aClientId)
            {
                p.isConnected = false;
                p.isReady = false;
                GameObject.Destroy(p.plyr);
                CheckRemainingPlayers();
            }
        }
    }

    void CheckRemainingPlayers() {
        int playersRemaining = 0;
        foreach (Player p in players) {
            if (p.plyr != null) {
                playersRemaining++;
            }
        }
        if (playersRemaining <= 0) {
            serverNet.CallRPC("EndRound", UCNetwork.MessageReceiver.AllClients, -1);
            foreach(Player p in players) {
                p.plyr = null;
                CheckIfPlayable();
            }
        }
    }
}
