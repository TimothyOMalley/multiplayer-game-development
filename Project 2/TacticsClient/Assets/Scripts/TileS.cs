﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class TileS : MonoBehaviour{

    public ExampleClient exClient;
    public int[] position = new int[2];

    private void Awake() {
        exClient = GameObject.FindGameObjectWithTag("ExampleClient").GetComponent<ExampleClient>();
    }

    public void GivePos(int x, int y) {
        position[0] = x;
        position[1] = y;
    }

    public void Run() {
        exClient.AttemptAction(position[0], position[1]);
    }
}
