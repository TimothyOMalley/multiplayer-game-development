﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class CircleTimer : MonoBehaviour{
    float timeLeft = 0;
    public Texture circle0,circle1,circle2,circle3,circle4,circle5,circle6,circle7,circle8,circle9;
    Texture[] circles;

    private void Awake() {
        circles = new Texture[10]{ circle0, circle1, circle2, circle3, circle4, circle5, circle6, circle7, circle8, circle9 };
    }

    void Update(){
        if(timeLeft > 1f) {
            timeLeft -= Time.deltaTime;
            int current = (int)Mathf.Floor(10 - timeLeft);
            if(current > 9) { current = 9; }
            GetComponent<RawImage>().texture = circles[current];
            //Debug.Log("current = circles[" + (int)Mathf.Floor(10 - timeLeft)+"]");
        }
    }

    public void Restart() {
        timeLeft = 10;
    }
}