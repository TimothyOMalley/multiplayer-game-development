﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class ExampleClient : MonoBehaviour
{
    public ClientNetwork clientNet;

    // Get the instance of the client
    static ExampleClient instance = null;
    
    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public GameObject loginScreen;
    public GameObject characterSelectScreen;
    public GameObject countDown;

    //public int playerId,playerTeam;

    // Singleton support
    public static ExampleClient GetInstance()
    {
        if (instance == null)
        {
            Debug.LogError("ExampleClient is uninitialized");
            return null;
        }
        return instance;
    }

    // Use this for initialization
    void Awake()
    {
        board.SetActive(false);
        loginScreen.SetActive(true);
        countDown.SetActive(false);
        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }
    }
    
    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;
        
        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);
    }

    // Update is called once per frame
    void Update()
    {
        /*
        timeToSend -= Time.deltaTime;
        if (timeToSend <= 0)
        {
            clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, 1, 1, "x");
            clientNet.CallRPC("Blah", UCNetwork.MessageReceiver.ServerOnly, -1, 1, 1, "x");
            timeToSend = 10;
        }
        */
    }

    public void UpdateState(int x, int y, string player)
    {
        // Update the visuals for the game
    }

    public void RPCTest(int aInt)
    {
        Debug.Log("RPC Test has been called with " + aInt);
    }

    public void NewClientConnected(long aClientId, string aValue)
    {
        Debug.Log("RPC NewClientConnected has been called with " + aClientId + " " + aValue);
    }

    // Networking callbacks
    // These are all the callbacks from the ClientNetwork
    void OnNetStatusNone()
    {
        Debug.Log("OnNetStatusNone called");
    }
    void OnNetStatusInitiatedConnect()
    {
        Debug.Log("OnNetStatusInitiatedConnect called");
    }
    void OnNetStatusReceivedInitiation()
    {
        Debug.Log("OnNetStatusReceivedInitiation called");
    }
    void OnNetStatusRespondedAwaitingApproval()
    {
        Debug.Log("OnNetStatusRespondedAwaitingApproval called");
    }
    void OnNetStatusRespondedConnect()
    {
        Debug.Log("OnNetStatusRespondedConnect called");
    }
    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        characterSelectScreen.SetActive(true);
        Debug.Log("OnNetStatusConnected called");

        clientNet.AddToArea(1);
    }

    void OnNetStatusDisconnecting()
    {
        Debug.Log("OnNetStatusDisconnecting called");
    }
    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");
        SceneManager.LoadScene("Client");
        
        loginInProcess = false;
    }
    public void OnChangeArea()
    {
        Debug.Log("OnChangeArea called");
    }

    // RPC Called by the server once it has finished sending all area initization data for a new area
    public void AreaInitialized()
    {
        Debug.Log("AreaInitialized called");
    }
    
    void OnDestroy()
    {
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }


    //I tried to put this in a different script but it kept saying the rpc call didn't exist.
    //GameSetup:
    public GameObject board;
    public GameObject tile;
    public GameObject[,] tiles;
    public Dictionary<int[], int> playerPos = new Dictionary<int[], int>();
    public Dictionary<int,PlayerInformation> playerInfo = new Dictionary<int, PlayerInformation>();
    int[] mapSize = new int[2];
    bool attacking,moving;

    public void SetMapSize(int x, int y) { //- Tell the client the size of the map
        board.SetActive(true);
        mapSize[0] = x; mapSize[1] = y;
        float width, height;
        width = (Screen.width/10)*8;
        height = (Screen.height/10)*9;
        Debug.Log(width + " x " + height);
        tiles = new GameObject[x, y];
        

        for(int col = 0; col < y; col++) {
            for(int row = 0; row < x; row++) {
                GameObject temp = Instantiate(tile, board.transform, false);
                temp.GetComponent<TileS>().GivePos(row, col);
                temp.GetComponent<RectTransform>().transform.localScale = new Vector3((width / x),(height / y), 0);
                temp.GetComponent<RectTransform>().transform.localPosition = new Vector3(((-width/2)-(width/8) +((width / x) * row))+((width/x)/2), ((-height/2)+(height/18f) +((height / y) * col))+((height/y)/2), 0);
                tiles[row, col] = temp;
            }
        }
    }
    public void GetPlayerInfo(Dictionary<int,PlayerInformation> plIn) { playerInfo = plIn; }

    public void SetBlockedSpace(int x, int y) {//- Tell the client that a specific space on the map is “blocked”
        tiles[x, y].GetComponent<RawImage>().color = Color.black;
    }

    public void SetPlayerPosition(int playerId, int x, int y) { //-A player has moved and is now at the position given
        tiles[playerInfo[playerId].XCord, playerInfo[playerId].YCord].GetComponent<RawImage>().texture = null;
        tiles[playerInfo[playerId].XCord, playerInfo[playerId].YCord].GetComponent<RawImage>().color = Color.white;
        tiles[x, y].GetComponent<RawImage>().texture = playerInfo[playerId].PlayerClass;
        if(playerInfo[playerId].TeamOne) {
            tiles[x, y].GetComponent<RawImage>().color = Color.blue;
        } else {
            tiles[x, y].GetComponent<RawImage>().color = Color.red;
        }
        playerPos.Remove(new int[2] { playerInfo[playerId].XCord, playerInfo[playerId].YCord });
        playerInfo[playerId].XCord = x;
        playerInfo[playerId].YCord = y;

        playerPos.Add(new int[2] { x, y }, playerId);

        if(playerId == myPlayerId) {

            if(movementHorToMake < 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord - 1, playerInfo[myPlayerId].YCord);
                movementHorToMake++;
            } else if(movementHorToMake > 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord + 1, playerInfo[myPlayerId].YCord);
                movementHorToMake--;
            } else if(movementVerToMake < 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord, playerInfo[myPlayerId].YCord - 1);
                movementVerToMake++;
            } else if(movementVerToMake > 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord, playerInfo[myPlayerId].YCord + 1);
                movementVerToMake--;
            }
        }
    }

    public CircleTimer circleTimer;

    public void StartTurn(int playerId) { //- Called when it is the start of the given player’s turn
        attack.gameObject.SetActive(false);
        move.gameObject.SetActive(false);
        pass.gameObject.SetActive(false);

        foreach(GameObject gameTile in tiles) {
            if(gameTile.GetComponent<RawImage>().color == Color.grey) { gameTile.GetComponent<RawImage>().color = Color.white; }
            if(gameTile.GetComponent<RawImage>().color == Color.magenta) { gameTile.GetComponent<RawImage>().color = Color.white; }
            if(gameTile.GetComponent<RawImage>().color == Color.blue + new Color(.5f, 0, 0)) { gameTile.GetComponent<RawImage>().color = Color.blue; }
            if(gameTile.GetComponent<RawImage>().color == Color.red + new Color(0, 0.629f, 0)) { gameTile.GetComponent<RawImage>().color = Color.red; }
        }
        if(playerInfo[playerId].TeamOne) {
            tiles[playerInfo[playerId].XCord, playerInfo[playerId].YCord].GetComponent<RawImage>().color = Color.blue + new Color(.5f, 0, 0);
        } else {
            tiles[playerInfo[playerId].XCord, playerInfo[playerId].YCord].GetComponent<RawImage>().color = Color.red + new Color(0, 0.629f, 0);
        }

        if(myPlayerId == playerId) {
            attack.gameObject.SetActive(true);
            move.gameObject.SetActive(true);
            pass.gameObject.SetActive(true);
            attacking = false;
            moving = false;

            circleTimer.Restart();
        }
    }

    public void AttackMade(int playerId, int x, int y) { //- The given player just made an attack at the given location
        //Update health also gets called, so unless I had an animation to play, which I don't, this method isn't required.
    }

    public void UpdateHealth(int playerId, int newHealth) { //- Update the health of a player, will be called whenever a player’s health changes
        playerInfo[playerId].Health = newHealth;
    }


    public Button attack, move, pass, team;
    public Text chatBox;
    private int myPlayerId;
    bool teamChat = false;

    public void GetPlayerId(int id_) {
        myPlayerId = id_;
    }

    public void PassTurn() {
        clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        attack.gameObject.SetActive(false);
        move.gameObject.SetActive(false);
        pass.gameObject.SetActive(false);
    }



    public void StartAttack() {
        int attackArea = playerInfo[myPlayerId].AttackDistance;
        int[] position = new int[2] { playerInfo[myPlayerId].XCord, playerInfo[myPlayerId].YCord };
        if(attacking) {
            foreach(GameObject gameTile in tiles) {
                if(gameTile.GetComponent<RawImage>().color == Color.magenta) {
                    gameTile.GetComponent<RawImage>().color = Color.white;
                }
            }
            move.gameObject.SetActive(true);
            attacking = false;
        } else {
            attacking = true;
            for(int rows = 0; rows < mapSize[0]; rows++) {
                for(int cols = 0; cols < mapSize[1]; cols++) {
                    if(Mathf.Abs(rows - position[0]) + Mathf.Abs(cols - position[1]) <= attackArea) {
                        if(tiles[rows, cols].GetComponent<RawImage>().color != Color.black) {
                            tiles[rows, cols].GetComponent<RawImage>().color = Color.magenta;
                        }
                    }
                }
            }
            move.gameObject.SetActive(false);
        }
    }

    public void StartMove() {
        int movement = playerInfo[myPlayerId].Movement;
        int[] position = new int[2] { playerInfo[myPlayerId].XCord, playerInfo[myPlayerId].YCord };
        if(moving) {
            foreach(GameObject gameTile in tiles) {
                if(gameTile.GetComponent<RawImage>().color == Color.grey) {
                    gameTile.GetComponent<RawImage>().color = Color.white;
                }
            }
            attack.gameObject.SetActive(true);
            moving = false;
        } else {
            moving = true;
            for(int rows = 0; rows < mapSize[0]; rows++) {
                for(int cols = 0; cols < mapSize[1]; cols++) {
                    if(Mathf.Abs(rows - position[0]) + Mathf.Abs(cols - position[1]) <= movement) {
                        if(tiles[rows, cols].GetComponent<RawImage>().color != Color.black) {
                            if(tiles[rows, cols].GetComponent<RawImage>().texture == null) {
                                tiles[rows, cols].GetComponent<RawImage>().color = Color.grey;
                            }
                        }
                    }
                }
            }
            attack.gameObject.SetActive(false);
        }
    }

    public int movementHorToMake, movementVerToMake;

    public void AttemptAction(int tileX,int tileY) {
        if(tiles[tileX,tileY].GetComponent<RawImage>().color == Color.magenta) {
            foreach(GameObject gameTile in tiles) {
                if(gameTile.GetComponent<RawImage>().color == Color.magenta) { gameTile.GetComponent<RawImage>().color = Color.white; }
            }
            clientNet.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, -1,tileX,tileY);
        }
        if(tiles[tileX, tileY].GetComponent<RawImage>().color == Color.grey) {
            foreach(GameObject gameTile in tiles) {
                if(gameTile.GetComponent<RawImage>().color == Color.grey) { gameTile.GetComponent<RawImage>().color = Color.white; }
            }
            movementHorToMake = tileX - playerInfo[myPlayerId].XCord;
            movementVerToMake = tileY - playerInfo[myPlayerId].YCord;

            if(movementHorToMake < 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord - 1, playerInfo[myPlayerId].YCord);
                movementHorToMake++;
            }else if(movementHorToMake > 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord + 1, playerInfo[myPlayerId].YCord);
                movementHorToMake--;
            }else if (movementVerToMake < 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord, playerInfo[myPlayerId].YCord-1);
                movementVerToMake++;
            }else if (movementVerToMake > 0) {
                clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, playerInfo[myPlayerId].XCord, playerInfo[myPlayerId].YCord+1);
                movementVerToMake--;
            }
        }
       
    }

    public void SendMessage() {
        if(teamChat) {
            clientNet.CallRPC("SendTeamChat", UCNetwork.MessageReceiver.ServerOnly, -1, chatBox.text);
        } else {
            clientNet.CallRPC("SendChat", UCNetwork.MessageReceiver.ServerOnly, -1, chatBox.text);
        }
    }

    public void ToggleTeamChat() { 
        if(teamChat) {
            teamChat = false;
            team.colors = new ColorBlock() { normalColor = Color.white };
        } else {
            teamChat = true;
            team.colors = new ColorBlock() { normalColor = Color.green };
        }
    }

    public GameObject theRealChatBox;

    public void DisplayChatMessage(string message) {
        theRealChatBox.GetComponent<Text>().text += message + "\n\n";
    }
}


