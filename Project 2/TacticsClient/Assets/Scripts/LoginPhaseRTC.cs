﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginPhaseRTC : MonoBehaviour {

    public GameObject b1p1, b1p2, b1p3, b2p1, b2p2, b2p3;
    GameObject countDown;
    Dictionary<int, Text[]> loginPlayers = new Dictionary<int, Text[]>();
    int playerId_, players;
    float timer, callDelay;
    bool starting = false;
    public Texture warrior, rogue, wizard;
    Dictionary<int, PlayerInformation> playerInfo = new Dictionary<int, PlayerInformation>();

    private void Awake() {
        countDown = GetComponentInParent<ExampleClient>().countDown;
    }

    public void SetPlayerId(int playerId) { playerId_ = playerId; }

    public void SetTeam(int team) { NewPlayerConnected(playerId_, team); ; }

    public void NewPlayerConnected(int playerId, int team) { //- Another player has connected to the game
        playerInfo.Add(playerId, new PlayerInformation(playerId, team));
        playerInfo[playerId].warrior = warrior;
        playerInfo[playerId].rogue = rogue;
        playerInfo[playerId].wizard = wizard;
        players++;
        Debug.Log("New player connected with player Id " + playerId + " and team " + team);
        if(team == 1) {
            if(b1p1 != null) {
                Text[] temp = b1p1.GetComponentsInChildren<Text>();
                if(temp[0].text.CompareTo("Class") == 0) {
                    loginPlayers.Add(playerId, new Text[2] { temp[1], temp[0] });
                } else {
                    loginPlayers.Add(playerId, new Text[2] { temp[0], temp[1] });
                }
                temp[0].color = Color.white;
                loginPlayers[playerId][0].text = "name";
                temp[1].color = Color.white;
                loginPlayers[playerId][1].text = "class";
                b1p1 = null;
            } else if(b1p2 != null) {
                Text[] temp = b1p2.GetComponentsInChildren<Text>();
                if(temp[0].text.CompareTo("Class") == 0) {
                    loginPlayers.Add(playerId, new Text[2] { temp[1], temp[0] });
                } else {
                    loginPlayers.Add(playerId, new Text[2] { temp[0], temp[1] });
                }
                temp[0].color = Color.white;
                temp[0].text = "";
                temp[1].color = Color.white;
                temp[1].text = "";
                b1p2 = null;
            } else if(b1p3 != null) {
                Text[] temp = b1p3.GetComponentsInChildren<Text>();
                if(temp[0].text.CompareTo("Class") == 0) {
                    loginPlayers.Add(playerId, new Text[2] { temp[1], temp[0] });
                } else {
                    loginPlayers.Add(playerId, new Text[2] { temp[0], temp[1] });
                }
                temp[0].color = Color.white;
                temp[0].text = "";
                temp[1].color = Color.white;
                temp[1].text = "";
                b1p3 = null;
            } else { Debug.Log("Too many players for team one"); }
        } else if(team == 2) {
            if(b2p1 != null) {
                Text[] temp = b2p1.GetComponentsInChildren<Text>();
                if(temp[0].text.CompareTo("Class") == 0) {
                    loginPlayers.Add(playerId, new Text[2] { temp[1], temp[0] });
                } else {
                    loginPlayers.Add(playerId, new Text[2] { temp[0], temp[1] });
                }
                temp[0].color = Color.white;
                temp[0].text = "name";
                temp[1].color = Color.white;
                temp[1].text = "class";
                b2p1 = null;
            } else if(b2p2 != null) {
                Text[] temp = b2p2.GetComponentsInChildren<Text>();
                if(temp[0].text.CompareTo("Class") == 0) {
                    loginPlayers.Add(playerId, new Text[2] { temp[1], temp[0] });
                } else {
                    loginPlayers.Add(playerId, new Text[2] { temp[0], temp[1] });
                }
                temp[0].color = Color.white;
                temp[0].text = "";
                temp[1].color = Color.white;
                temp[1].text = "";
                b2p2 = null;
            } else if(b2p3 != null) {
                Text[] temp = b2p3.GetComponentsInChildren<Text>();
                if(temp[0].text.CompareTo("Class") == 0) {
                    loginPlayers.Add(playerId, new Text[2] { temp[1], temp[0] });
                } else {
                    loginPlayers.Add(playerId, new Text[2] { temp[0], temp[1] });
                }
                temp[0].color = Color.white;
                loginPlayers[playerId][0].text = "";
                temp[1].color = Color.white;
                loginPlayers[playerId][1].text = "";
                b2p3 = null;
            } else { Debug.Log("Too many players for team two"); }
        }
    }

    public void PlayerNameChanged(int playerId, string name) { //- Another player has changed their name
        loginPlayers[playerId][0].text = name;
    }

    public void PlayerIsReady(int playerId, bool isReady) { //- Another player is ready to play
        if(isReady) {
            loginPlayers[playerId][0].color = Color.green;
            loginPlayers[playerId][1].color = Color.green;
        } else {
            loginPlayers[playerId][0].color = Color.white;
            loginPlayers[playerId][1].color = Color.white;
        }
    }

    public void PlayerClassChanged(int playerId, int type) { //- Another player has changed their character type
        playerInfo[playerId].setClass(type);
        switch(type) {
            case 1:
                loginPlayers[playerId][1].text = "Warrior";
                break;
            case 2:
                loginPlayers[playerId][1].text = "Rogue";
                break;
            case 3:
                loginPlayers[playerId][1].text = "Wizard";
                break;
        }
    }

    private void Update() {
        if(!starting) {
            if(players >= 2) {
                foreach(Text[] arrays in loginPlayers.Values) {
                    foreach(Text textElement in arrays) {
                        if(textElement.color != Color.green) {
                            return;
                        }
                    }
                }
                if(callDelay <= 0) {
                    GetComponentInParent<ClientNetwork>().CallRPC("AttemptGameStart", UCNetwork.MessageReceiver.ServerOnly, -1);
                    callDelay = 10;
                } else {
                    callDelay -= Time.deltaTime;
                }
            }
        } else {
            countDown.GetComponentInChildren<Text>().text = Mathf.Ceil(timer).ToString();
            timer -= Time.deltaTime;
            if(timer <= 0) { countDown.SetActive(false); }
        }

    }

    public void GameStart(int time) { //- The game will start after the given amount of time
        Debug.Log("Start of game called in T-" + time);
        timer = time;
        starting = true;
        GetComponentInParent<ExampleClient>().characterSelectScreen.SetActive(false);
        countDown.SetActive(true);
        GetComponentInParent<ExampleClient>().GetPlayerInfo(playerInfo);
        GetComponentInParent<ExampleClient>().GetPlayerId(playerId_);
    }
}
