﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Entry{
    private int playerId,teamNum;
    private GameObject uiEntry;
    private Text UIName,UIClass;
    private string name;
    private bool ready;

    public Entry(int playerId_,int team, GameObject uiElement) {
        playerId = playerId_;
        teamNum = team;
        uiEntry = uiElement;
        name = "";
        Text[] temp = uiElement.GetComponentsInChildren<Text>();
        Debug.Log(temp[0] + " " + temp[1]);
        if(temp[0].text == "Name") { UIName = temp[0]; UIClass = temp[1]; } else { UIName = temp[1]; UIClass = temp[0]; }
    }

    public void SetName(string name_) {
        name = name_;
        Update();
    }

    private void Update() {
        UIName.text = name;
    }
}
