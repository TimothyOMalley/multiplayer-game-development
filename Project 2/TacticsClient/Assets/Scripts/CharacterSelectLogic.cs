﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectLogic : MonoBehaviour{
    public GameObject characterSelect; 
    public Text playerName;
    private int classNum = 1;
    private string playerNameText = "";
    public Button warrior,rogue,wizard,ready;
    public ClientNetwork clientNet;
    bool currentlyReady = false;

    private void Update() {
        if(playerName.text != playerNameText) {
            clientNet.CallRPC("SetName", UCNetwork.MessageReceiver.ServerOnly, -1, playerName.text);
            playerNameText = playerName.text;
        }
    }

    public void SwapClass(int classNum_) { 
        switch(classNum) {
            case 1:
                warrior.interactable = false;
                rogue.interactable = true;
                wizard.interactable = true;
                classNum = classNum_;
                break;
            case 2:
                warrior.interactable = true;
                rogue.interactable = false;
                wizard.interactable = true;
                classNum = classNum_;
                break;
            case 3:
                warrior.interactable = true;
                rogue.interactable = true;
                wizard.interactable = false;
                classNum = classNum_;
                break;
        }
        clientNet.CallRPC("SetCharacterType", UCNetwork.MessageReceiver.ServerOnly, -1, (int) classNum);
    }

    public void ReadyUp() {
        if(currentlyReady) {
            clientNet.CallRPC("Ready", UCNetwork.MessageReceiver.ServerOnly, -1, false);
            currentlyReady = false;
        } else {
            clientNet.CallRPC("Ready", UCNetwork.MessageReceiver.ServerOnly, -1, true);
            currentlyReady = true;
        }
        Debug.Log("Toggled ready to "+currentlyReady);
    }
}
