﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PlayerInformation {
    int playerId, health, movement, attackDamage, attackDistance, xCord, yCord;
    bool teamOne;
    public Texture playerClass,warrior,rogue,wizard;


    public PlayerInformation(int id, int teamNum) {
        playerId = id;
        teamOne = (teamNum == 1);
        xCord = 0;
        yCord = 0;
    }

    public void setClass(object pc) {
        switch(pc) {
            case "Warrior":
            case 1:
                health = 100;
                movement = 2;
                attackDamage = 30;
                attackDistance = 1;
                playerClass = warrior;
                break;
            case "Rogue":
            case 2:
                health = 70;
                movement = 5;
                attackDamage = 10;
                attackDistance = 1;
                playerClass = rogue;
                break;
            case "Wizard":
            case 3:
                health = 30;
                movement = 4;
                attackDamage = 10;
                attackDistance = 6;
                playerClass = wizard;
                break;
        }
    }

    public int Health {
        get { return health; }
        set { health = value; }
    }

    public int Movement {
        get { return movement; }
    }

    public int AttackDamage {
        get { return attackDamage; }
    }

    public int AttackDistance {
        get { return attackDistance; }
    }

    public Texture PlayerClass {
        get { return playerClass; }
    }

    public bool TeamOne {
        get { return teamOne; }
    }

    public int XCord {
        get { return xCord; }
        set { xCord = value; }
    }

    public int YCord {
        get { return yCord; }
        set { yCord = value; }
    }
}
