﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class Tag : MonoBehaviour { //this is the client

    public ClientNetwork clientNet;
    public int clientId;
    float hitBoxX, hitBoxY, memoryHitX, memoryHitY;
    int playerId;
   

    public void SetPlayerId(int playerId_) {
        playerId = playerId_;
    }

    private void Awake() {
        hitBoxX = gameObject.transform.localScale.x;
        hitBoxY = gameObject.transform.localScale.y;
    }

    public void MakeTheCall() { //this was a workaround to get things working a little smoother with client Ids
        clientNet = GetComponent<NetworkSync>().clientNet;
        clientId = GetComponent<NetworkSync>().GetId();
        clientNet.CallRPC("GiveClientId",UCNetwork.MessageReceiver.ServerOnly, -1, clientId, playerId);
    }

    public void SetPlayerAsIt(bool it) { //this sets a player as it, or not it, changes their color for all clients, and changes their speed.
        if (it) {
            clientNet = GetComponent<NetworkSync>().clientNet;
            clientId = GetComponent<NetworkSync>().GetId();
            clientNet.CallRPC("ChangeColor", UCNetwork.MessageReceiver.AllClients, clientId, 1f, 0f, 0f);
            gameObject.GetComponent<Player>().speed = 5.5f;
        } else {
            clientNet = GetComponent<NetworkSync>().clientNet;
            clientId = GetComponent<NetworkSync>().GetId();
            clientNet.CallRPC("ChangeColor", UCNetwork.MessageReceiver.AllClients, clientId, 0.5f,0.5f,0.5f);
            gameObject.GetComponent<Player>().speed = 5;
        }
    }

    public void ChangeColor(float r, float g, float b) { //the Color object can't be passed via RPC call so I just did RGB as floats.
        gameObject.GetComponent<Renderer>().material.color = new Color(r,g,b,1);
    }

    public float msgCooldown = 0;

    private void Update() {

        if ((hitBoxX != memoryHitX) || (hitBoxY != memoryHitY)) { //if the hitbox ever changes, tell the server.
            memoryHitX = hitBoxX; //remember what the hitbox is 
            memoryHitY = hitBoxY;
            clientNet.CallRPC("GetCharacterHitBox", UCNetwork.MessageReceiver.ServerOnly, -1, hitBoxX, hitBoxY, playerId);
        }
        if (msgCooldown == 0) {
            //updates the position that will be used to determine if players are near one another.
            clientNet.CallRPC("UpdatePosition", UCNetwork.MessageReceiver.ServerOnly, playerId, gameObject.transform.position.x, gameObject.transform.position.y);
            msgCooldown = 0.4f;
        } else {
            msgCooldown -= Time.deltaTime;
            if(msgCooldown < 0) { msgCooldown = 0; }
        }
    }
}
