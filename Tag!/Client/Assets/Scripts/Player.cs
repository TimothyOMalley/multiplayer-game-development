﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float speed = 5;
    public float additionalSpeed = 0;
    public float cooldown = 0;
    public float sprintTime = 0;

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NetworkSync>().owned)
        {
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * (speed + additionalSpeed) * Time.deltaTime, 0);
            transform.position += movement;

            if (Input.GetButtonDown("Jump")) {
                if(cooldown == 0) { //if not on cooldown, start two timers.
                    additionalSpeed = 0.5f;
                    sprintTime = 3;
                    cooldown = 5;
                }
            }
            if (sprintTime > 0) { //if sprinting
                sprintTime -= Time.deltaTime; //subtract time until
                if (sprintTime <= 0) { //sprint is equal to or less than 0, then set speed back to normal
                    additionalSpeed = 0f;
                    sprintTime = 0;
                }
            } else if (cooldown > 0) {//during the sprint timer, the cooldown timer doesn't run
                cooldown -= Time.deltaTime;
                if (cooldown < 0) {
                    cooldown = 0; //once the cooldown is over sprint can be used again
                }
            }
        }
    }
}
