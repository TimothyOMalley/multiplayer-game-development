﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SocialPlatforms;

/*methods for client:
 * SetPlayerAsIt(playerId id, bool it_);
 * SetPlayerId(int playerId id);
 */



public class Tag : MonoBehaviour{ //This is the server's 

    float levelSizeX = 23.91572f; //the horizontal length of a level
    float levelSizeY = 12.9591f; // the veritcal height of a level

    //instance used for server initialization
    public static Tag instance;

    //instance of the server network
    public ServerNetwork serverNet;

    //port the server is running on
    public const int portNumber = 603;

    [System.Serializable]
    public class Player{

        public long clientId; // the clien't Id number
        public bool isConnected; //Information to know if the player has disconnected or not
        public int realClientId;

        public int playerId; //Assign player id
        public bool it; //if the player is "it" or not

        public float xPosition; //the horizontal position of a player
        public float yPosition; //the vertical position of a player
        public float hitBoxX = 0.5f; //get the horizontal and vertical offset of the user's hitbox to their position.
        public float hitBoxY = 0.5f;

        public float noTagBackCounter = 0;
    }

    public List<Player> players = new List<Player>(); //list of currently connected players
    static int lastPlayerId = 0;

    private void Awake(){

        instance = this;

        //Initalization of the server network
        ServerNetwork.port = portNumber;
        if(serverNet == null){
            serverNet = GetComponent<ServerNetwork>();
        }
        if(serverNet == null){
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

    }

    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data){

        Debug.Log("Connection request from IP " + data.connection.RemoteEndPoint.Address);

        Player newPlayer = new Player();
        newPlayer.clientId = data.id;
        newPlayer.isConnected = false;
        newPlayer.xPosition = 0;
        newPlayer.yPosition = 0;
        players.Add(newPlayer);

        serverNet.ConnectionApproved(data.id);
    }

    void OnClientConnected(long aClientId){ //when a client connects

        Player newPlayer = null;
        bool thereIsntAnIt = true;
        float spawnPosX = 0; //define spawn position of new player
        float spawnPosY = 0;

        foreach(Player p in players){ //now go through every player in the game
            if (p.it)
                thereIsntAnIt = false;
            if(p.clientId == aClientId){ //and determine that their clientId is in the game

                p.isConnected = true; //say they're connected
                p.playerId = ++lastPlayerId; //give the player a player id
                serverNet.CallRPC("SetPlayerId", aClientId, -1, p.playerId); //give the client it's player Id

                while (true) { //now enter a loop;

                    bool conflicting = false; 
                    foreach (Player users in players) { //for each player in the game
                        if (users.clientId != p.clientId) { //except for the connecting player,
                            if (users.xPosition == spawnPosX) { //check and make sure that the new player won't 
                                if (users.yPosition == spawnPosY) { // spawn in or ontop of another player
                                    conflicting = true; //if so; mark that position as conflicting
                                }
                            }
                        }
                    }
                    if (conflicting) { //if the spawn position is conflicting, make a new spawn position
                        spawnPosX = UnityEngine.Random.Range(0, levelSizeX); //that fits within the level constraints
                        spawnPosY = UnityEngine.Random.Range(0, levelSizeY);
                    } else {
                        break; //otherwise; if the position is not conflicting, break the loop.
                    }
                }

                //now that we know that the player spawn position isn't conflicting, set that to the player's position
                p.it = thereIsntAnIt;
                p.xPosition = spawnPosX;
                p.yPosition = spawnPosY;
                newPlayer = p;
                Debug.Log("Setting Player Starting Position");
                serverNet.CallRPC("SetPlayerPos", p.clientId, -1, new Vector2(spawnPosX, spawnPosY));
            }

            if (newPlayer == null) { //final catch to make sure the player is set to something
                Debug.Log("OnClientConnected: Unable to find unknown player for client " + aClientId);
                return;
            }
        }
    }

    public void GiveClientId(int client, int playerId) {
        foreach(Player p in players) {
            if(p.playerId == playerId) {
                p.realClientId = client;
                serverNet.CallRPC("SetPlayerAsIt", UCNetwork.MessageReceiver.AllClients, client, p.it);
            }
        }
    }

    public void UpdatePosition(int playerId, float xPos, float yPos) {
        foreach(Player p in players) {
            if(p.playerId == playerId) {
                p.xPosition = xPos;
                p.yPosition = yPos;
            }
        }
    }

    // A client has disconnected
    void OnClientDisconnected(long aClientId) {
        // Set the isConnected to false on the player
        Player p = GetPlayerByClientId(serverNet.SendingClientId);
        if (p == null) return;

        p.isConnected = false; //disconnect player
        players.Remove(p); //remove player from player lis

        if (p.it) { //if the player that left was it, set a new player as it by random from remaning players
            serverNet.CallRPC("SetPlayerAsIt", UCNetwork.MessageReceiver.AllClients, players[UnityEngine.Random.Range(0, players.Count)].playerId, true);
        }
    }

    // Get the player for the given client id
    Player GetPlayerByClientId(long aClientId) {
        for (int i = 0; i < players.Count; i++) {
            if (players[i].clientId == aClientId) {
                return players[i];
            }
        }

        // If we can't find the player for this client, who are they? kick them
        Debug.Log("Unable to get player for unknown client " + aClientId);
        serverNet.Kick(aClientId);
        return null;
    }

    public void GetCharacterHitBox(float x, float y, long playerId) {
        foreach(Player p in players){
           if(p.playerId == playerId) {
                p.hitBoxX = x / 2;
                p.hitBoxY = y / 2;
            }
        }
    }

    private void Update() {
        foreach(Player it in players) {
            if (it.noTagBackCounter > 0) { //if the player is currently on no-tag bag; count that down
                it.noTagBackCounter -= Time.deltaTime;
                if (it.noTagBackCounter < 0)
                    it.noTagBackCounter = 0; //if the player's no-tag back is less than zero make it zero.
            }
            if (it.it) { //if the player is "it"
                foreach (Player tagged in players) { //then check every player in the game;
                    bool insideX = false;
                    bool insideY = false;
                    if (tagged.it) { } else { //you can't tag yourself
                        //if the player's hitbox is touching "it's" hitbox, then make a successful tag.
                        if (((it.xPosition - it.hitBoxX) > (tagged.xPosition - tagged.hitBoxX)) && ((it.xPosition - it.hitBoxX) < (tagged.xPosition + tagged.hitBoxX))) {
                            //if the left side of "it" is in between the left and right sides of "tagged"
                            insideX = true; //make insideX true
                        } else if (((it.xPosition + it.hitBoxX) > (tagged.xPosition - tagged.hitBoxX)) && ((it.xPosition - it.hitBoxX) < (tagged.xPosition + tagged.hitBoxX))) {
                            //OR if the right side of "it" is in between the left and right sides of "tagged"
                            insideX = true; //make insideX true
                        }
                        if (insideX) { //this is to make the computer not have to do extra math to hopefully help with processing power for the server when there's a lot of players
                            if (((it.yPosition - it.hitBoxY) > (tagged.yPosition - tagged.hitBoxY)) && ((it.yPosition - it.hitBoxY) < (tagged.yPosition + tagged.hitBoxY))) {
                                //if the bottom of "it" is inside the bottom and top of "tagged"
                                insideY = true; //make insideY true
                            } else if (((it.yPosition + it.hitBoxY) > (tagged.yPosition - tagged.hitBoxY)) && ((it.yPosition - it.hitBoxY) < (tagged.yPosition + tagged.hitBoxY))) {
                                //OR if the top of "it" is inside the bottom and top of "tagged"
                                insideY = true; //make insideY true
                            }
                            if (insideY) {
                                //lastly, if the "it" player's hit box is colliding with the hitbox of the player, then make the player it
                                if (tagged.noTagBackCounter == 0) { //if the player can be tagged;
                                    serverNet.CallRPC("SetPlayerAsIt", UCNetwork.MessageReceiver.AllClients, it.realClientId, false); //swap who's it
                                    it.it = false;
                                    it.noTagBackCounter = 5; //give them a no tag back cooldown
                                    serverNet.CallRPC("SetPlayerAsIt", UCNetwork.MessageReceiver.AllClients, tagged.realClientId, true); ;
                                    tagged.it = true;
                                    Update(); //cut to Update to ensure that the game doesn't make anyone else "tagged" based on their collision with the previous "it"
                                }
                            }
                        }
                    }
                }
            }
        }
    }


}
